package com.marcelbrilha.workshopmongo.resource;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.marcelbrilha.workshopmongo.domain.Post;
import com.marcelbrilha.workshopmongo.resource.util.URL;
import com.marcelbrilha.workshopmongo.services.PostService;

@RestController
@RequestMapping(value="/posts")
public class PostResource {
	
	@Autowired
	private PostService postService;

	@GetMapping(value="/{id}")
	public ResponseEntity<Post> findById(@PathVariable String id) {
		Post post = postService.findById(id);
		return ResponseEntity.ok().body(post);
	}
	
	@GetMapping(value="/title-search")
	public ResponseEntity<List<Post>> findByTitle(@RequestParam(value="title", defaultValue="") String title) {
		title = URL.decodeParam(title);
		List<Post> posts = postService.findByTitle(title);
		return ResponseEntity.ok().body(posts);
	}
	
	@GetMapping(value="/full-search")
	public ResponseEntity<List<Post>> fullSearch(
		@RequestParam(value="text", defaultValue="") String text,
		@RequestParam(value="minDate", defaultValue="") String minDate,
		@RequestParam(value="maxDate", defaultValue="") String maxDate
	) {
		
		text = URL.decodeParam(text);
		Date dateMin = URL.converDate(minDate, new Date(0L));
		Date dateMax = URL.converDate(maxDate, new Date());
		
		List<Post> posts = postService.fullSearch(text, dateMin, dateMax);
		return ResponseEntity.ok().body(posts);
	}
}
